import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Button} from 'react-native-elements';


const Submit = props => {

    signIn = async () => {
        //  const response = await api.post('/api/usuario/login',{
       //      Usuario: 'fjacobe',
        //      Password: '123456'        
       //   });
          Alert.alert(
              'Bienvenido oe GIL'
           )
          ///const { user, token} = response.data//
      };


    return (
        <TouchableOpacity style={[styles.container,{backgroundColor: props.color}]} onPress={props.onPress}>
                <Text style={styles.submitText}>{props.title}</Text>
        </TouchableOpacity>

    )

}

const styles = StyleSheet.create({
    container: {
        width: '90%',
        height: 50,
        borderColor: 'blue',
        borderRadius: 10,
        marginVertical: 10,
        borderWidth: 0
    },
    submitText:{
        fontSize: 22,
        fontWeight: 'bold',
        color: 'white',
        alignSelf: 'center',
        marginVertical: 10
    }

});

export default Submit;