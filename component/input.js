import React, {Component} from 'react';
import { View,StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Input} from 'react-native-elements';


class Inputs extends Component {
    state = {isFocused: false}

    onFocusChange = () => {
        this.setState({isFocused: true});

    }
    
    render(){
        return(
        <View style={[styles.container,{borderColor: this.stateisFocused ? 
        "#1852A1": "#1852A1"}]}>
                <Input 
                placeholder={this.props.name}
                onFocus={this.onFocusChange}
                inputContainerStyle={styles.inputContainer}
                inputStyle={styles.inputText}
                secureTextEntry={this.props.pass}
                onChangeText={(val) => this.props.onChangeText(val)}
                leftIcon={
                    <Icon 
                    name='Password'
                        size={30}

                    />

                }
                />
                
        </View>
        
        );
    }


}

const styles= StyleSheet.create({
    container:{
        width: '90%',
        height: 50,
        borderRadius: 10,
        marginVertical: 10,
        borderWidth: 3.5
    },
    inputContainer:{
      borderBottomWidth: 0  
    },
    inputText: {
        
        marginLeft: 5
    }
});

export default Inputs;