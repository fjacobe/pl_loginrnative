import { create }  from 'apisauce';

const api = create({
    baseURL: 'https://localhost:44373',
});

export default api;